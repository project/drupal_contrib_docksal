# Docksal powered Drupal Core Contribution Installation

This is a Drupal 8 installation geared for local Core and Contrib development
for use with Docksal.

Visit the [module homepage](https://www.drupal.org/project/drupal_contrib_docksal) for installation intructions.  