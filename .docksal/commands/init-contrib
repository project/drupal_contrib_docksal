#!/usr/bin/env bash

## Configure the settings for Drupal.org
##
## Usage: fin init-contrib

# Abort if anything fails
set -e

#-------------------------- Reset local settings --------------------------------
# Reset all variables in local docksal.env

fin config rm "SETUP_TYPE" --env=local > /dev/null 2>&1
fin config rm "CONTRIB_PROJECT_REPO" --env=local > /dev/null 2>&1
fin config rm "CONTRIB_PROJECT_ISSUE_FORK" --env=local > /dev/null 2>&1
fin config rm "CONTRIB_PROJECT_NAME" --env=local > /dev/null 2>&1
fin config rm "CONTRIB_PROJECT_ISSUE" --env=local > /dev/null 2>&1

#-------------------------- END: Reset local settings --------------------------------

#-------------------------- Helper functions --------------------------------

# Ascertain the type of project that the Docksal instance should be setup with.
# This is the main logic tree and
setup_project ()
{
  printf "\n"
  read -p "Enter 'core', 'module' or 'theme' (enter 'skip' for bare-bones setup): " typevar

  if [[ "$typevar" == "module" ]]
  then
    set_setup_type "MODULE"
    printf "\n"
    echo 'What is the name of the Drupal project?'
    set_contrib_project
  elif [[ "$typevar" == "core" ]]
  then
    set_setup_type "CORE"
   elif [[ "$typevar" == "theme" ]]
    then
      set_setup_type "THEME"
      printf "\n"
      echo 'What is the name of the Drupal theme?'
      set_contrib_project
  elif [[ "$typevar" == "skip" ]]
    then
      set_setup_type "BAREBONES"
  else
    printf "\n"
    echo "Invalid option '$typevar' needs to be 'core', 'module', 'theme' or 'skip'. Try again..."
    setup_project
  fi
}

# This will setup the necessary info to provision a Drupal.org contrib project setup
set_contrib_project()
{
  printf "\n"
  read -p "Enter project name: " project_name

  fin config set "CONTRIB_PROJECT_NAME=${project_name}" --env=local > /dev/null 2>&1
  fin config set "CONTRIB_PROJECT_REPO=git@git.drupal.org:project/${project_name}.git" --env=local > /dev/null 2>&1

  printf "\n"
  echo "What issue are you working on?"
  set_issue

  printf "\n"
  echo -e "Does this project have an issue fork setup for the issue you're working on?"
  set_issue_fork ${project_name}

}

# Set the current issue number being worked on.
set_issue()
{

  printf "\n"
  read -p "Enter the issue number from Drupal.org: " issue_number

  re='^[0-9]+$'
  if ! [[ $issue_number =~ $re ]] ;
    then
      printf "\n"
      echo "Invalid option. Issue number is a number"
      set_issue
    else
      fin config set "CONTRIB_PROJECT_ISSUE=${issue_number}" --env=local > /dev/null 2>&1
  fi

}


set_issue_fork()
{
  printf "\n"
  read -p "Enter [y/n]: " has_issue_fork

  if [[ "$has_issue_fork" == "y" ]]
  then
    fin config set "CONTRIB_PROJECT_ISSUE_FORK=git@git.drupal.org:issue/${1}-${issue_number}.git" --env=local > /dev/null 2>&1
  elif [[ "$has_issue_fork" != "n" ]]
  then
     echo "Invalid option try again"
     set_issue_fork ${1}
  fi
}

# Sets
set_setup_type ()
{
  fin config set "SETUP_TYPE=${1}" --env=local > /dev/null 2>&1
}
#-------------------------- END: Helper functions --------------------------------

#-------------------------- Execution --------------------------------

printf '\n'

echo -e "Is this for Drupal core or a contrib module?"
setup_project

printf '\n'
#-------------------------- END: Execution --------------------------------