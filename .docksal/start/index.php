<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">

<style>
    a {
        color: black;
    }

    a:hover {
        text-decoration: underline;
    }

    .card {
        background: white;
        border-radius: 0.5rem;
        box-shadow: 0 1px 3px 0 rgb(0 0 0 / 10%), 0 1px 2px 0 rgb(0 0 0 / 6%);
        padding: 1rem;
    }

    .title {
        text-transform: uppercase;
        font-weight: 600;
        font-size: 0.875rem;
        letter-spacing: 0.025em;
        color: #69778c;
    }
</style>

<head>
    <title>Dashboard</title>
</head>

<body style='
    font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif;
    font-size: 18px;
    background: #f0f2fd;
'>

<div style="
    max-width: 60rem;
    margin: 0 auto;
    padding: 2rem;
">
    <h1>Dashboard</h1>
    <H2>Issue information</H2>
    <div class="card">
        <p><strong>Project type - </strong><?php echo strtolower(getenv('SETUP_TYPE')) ?>
        </p>
        <?php if (getenv('SETUP_TYPE') !== "CORE" && getenv('SETUP_TYPE') !== "BAREBONES"): ?>
            <p><strong>Project
                    - </strong><?php echo getenv('CONTRIB_PROJECT_NAME') ?></p>
            <p><strong>Issue number
                    - </strong><a href="https://www.drupal.org/project/<?php echo getenv('CONTRIB_PROJECT_NAME') ?>/issues/<?php echo getenv('CONTRIB_PROJECT_ISSUE') ?>" target="_blank"> #<?php echo getenv('CONTRIB_PROJECT_ISSUE') ?></a>
            </p>
            <p><strong>Issue fork?
                    - </strong><?php echo getenv('CONTRIB_PROJECT_ISSUE_FORK') ? 'Yes' : 'No'; ?></p>
            <p><strong>Code location - </strong><?php echo getenv('DOCROOT') ?>/<?php echo strtolower(getenv('SETUP_TYPE')) ?>s/contrib/<?php echo getenv('CONTRIB_PROJECT_NAME') ?>
            </p>
        <?php endif ?>
    </div>
    <H2>Services</H2>
    <div style="
    grid-template-columns: 1fr 1fr;
    grid-gap: 2rem;
    display: grid;
  ">
        <div id="web" class="card">
          <p class="title">
            Web
          </p>
          <p>
              <strong>Site URL - </strong>
              <a href="<?php echo $_SERVER['REQUEST_SCHEME'] . '://' . getenv('VIRTUAL_HOST') ?>"
                 target="_blank"
              >
                <?php echo $_SERVER['REQUEST_SCHEME'] . '://' . getenv('VIRTUAL_HOST') ?>
              </a>
          </p>
            <p><strong>Directory
                    - </strong><?php echo getenv('DOCROOT') ?></p>
            <p><strong>Username
                    - </strong>admin</p>
        </div>
        <div class="card">
            <p class="title">
                DB
            </p>
            <p><strong>Database
                    - </strong><?php echo getenv('MYSQL_DATABASE') ?></p>
            <p><strong>Host - </strong><?php echo getenv('MYSQL_HOST') ?></p>
            <p><strong>Username - </strong><?php echo getenv('MYSQL_USER') ?>
            </p>
            <p><strong>Password
                    - </strong><?php echo getenv('MYSQL_PASSWORD') ?></p>
        </div>
    </div>

    <H2>System info</H2>

    <div class="card">
        <p><strong>PHP Version - </strong><?php echo getenv('PHP_VERSION') ?>
        </p>
        <p><strong>Composer Version
                - </strong><?php echo getenv('COMPOSER2_VERSION') ?></p>
        <p><strong>XDebug
                - </strong><?php echo getenv('XDEBUG_ENABLED') ? 'Enabled' : 'Disabled'; ?>
        </p>
        <p><strong>Drush Version
                - </strong><?php echo getenv('DRUSH_VERSION') ?></p>
        <p><strong>Node Version - </strong><?php echo getenv('NODE_VERSION') ?>
        </p>
        <!--        --><?php //phpinfo() ?>
    </div>

</div>
</body>
</html>